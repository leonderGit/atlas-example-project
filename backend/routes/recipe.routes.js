const express = require("express");
const router = express.Router();
const {
  getRecipes,
  addRecipe,
} = require("../controllers/recipe.controller.js");

router.get("/", getRecipes);
router.post("/", addRecipe);

module.exports = router;
