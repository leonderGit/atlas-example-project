require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const recipeRouter = require("./backend/routes/recipe.routes.js");
const path = require("path");

const app = express();
app.use(express.static(path.join(__dirname, "frontend")));
const port = 3000;

mongoose
  .connect(process.env.MONGO_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Connected to MongoDB Atlas"))
  .catch((err) => console.error("Could not connect to MongoDB Atlas:", err));

app.use(express.json());
app.use("/api/recipes", recipeRouter);

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
  console.log(`Visit: http://localhost:${port}`);
});
