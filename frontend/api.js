async function fetchRecipes() {
  const response = await fetch("/api/recipes");
  return response.json();
}

async function addRecipe(recipe) {
  const response = await fetch("/api/recipes", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(recipe),
  });
  return response.json();
}

export { fetchRecipes, addRecipe };
