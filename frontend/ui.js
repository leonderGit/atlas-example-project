import { fetchRecipes, addRecipe } from "./api.js";

document
  .getElementById("recipe-form")
  .addEventListener("submit", async function (event) {
    event.preventDefault();
    const name = document.getElementById("recipe-name").value;
    const ingredients = document
      .getElementById("recipe-ingredients")
      .value.split(",");
    const instructions = document.getElementById("recipe-instructions").value;

    const recipe = { name, ingredients, instructions };
    const addedRecipe = await addRecipe(recipe);
    displayRecipe(addedRecipe);
    document.getElementById("recipe-form").reset();
  });

async function loadRecipes() {
  const recipes = await fetchRecipes();
  recipes.forEach((recipe) => displayRecipe(recipe));
}

function displayRecipe(recipe) {
  const listElement = document.createElement("li");

  // Create a header for the recipe name
  const header = document.createElement("h3");
  header.textContent = recipe.name;
  listElement.appendChild(header);

  // Create a paragraph for ingredients
  const ingredientsText = document.createElement("p");
  ingredientsText.textContent = `Ingredients: ${recipe.ingredients.join(", ")}`;
  listElement.appendChild(ingredientsText);

  // Create a paragraph for instructions
  const instructionsText = document.createElement("p");
  instructionsText.textContent = `Instructions: ${recipe.instructions}`;
  listElement.appendChild(instructionsText);

  document.getElementById("recipes-list").appendChild(listElement);
}

loadRecipes();
